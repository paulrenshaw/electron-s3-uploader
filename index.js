const { app, BrowserWindow } = require('electron')
const { join } = require ('path')

function createWindow () {
    let win = new BrowserWindow({
        height: 1024,
        width: 1280,
        zoomToPageWidth: true,
        webPreferences: {
            preload: join(__dirname, 'preload.js')
        }
    })
    win.loadFile('index.html')
    if (process.env.NODE_ENV == 'development') win.webContents.openDevTools()
}

app.whenReady().then(createWindow)