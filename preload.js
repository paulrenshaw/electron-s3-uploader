require('dotenv-safe').config()
const AWS = require('aws-sdk')
const chokidar = require('chokidar')
const { readFileSync } = require('fs')

const s3 = new AWS.S3({
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID || '',
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || ''
    }
})

window.addEventListener('DOMContentLoaded', () => {
    const app = window.app

    window.startWatcher = (folder, { bucket, prefix }) => {
        const _folder = folder.length == '/' ? folder.slice(0, folder.length - 1) : folder
        app.logs.push(`watching ${_folder}`)
        window.watcher = chokidar.watch(_folder, { awaitWriteFinish: true, ignoreInitial: true }).on('add', (path, stats) => {
            const filePath = path.replace(_folder, '')
            app.logs.push(`file added: ${filePath}`)
            try {
                const file = readFileSync(path)
                const awsKey = `${prefix}${filePath}`
                s3.putObject({
                    Body: file,
                    Bucket: bucket,
                    Key: awsKey
                }, (err) => {
                    if (err) throw err
                    else {
                        app.logs.push(`Successfully uploaded to s3://${bucket}/${awsKey}`)
                    }
                })
            } catch (err) {
                app.logs.push(JSON.stringify(err))
            }
        })
        app.running = true
    }
})